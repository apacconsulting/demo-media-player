//
//  ViewController.swift
//  MediaPlayer
//
//  Created by Vinh Thien on 11/27/16.
//  Copyright © 2016 VinhThien. All rights reserved.
//

import UIKit
import AVFoundation
import CoreMedia

class ViewController: UIViewController, AVAudioPlayerDelegate {
    
    var player:AVPlayer?
    var playerItem:AVPlayerItem?
    
    var count = 0
    var indexTime = 0
    var indexAudio = 0;
    var timer = Timer()
    var listAudio = ["http://appeducation.esy.es/nhac.mp3","http://appeducation.esy.es/nhac1.mp3","http://appeducation.esy.es/nhac2.mp3"]
    

    @IBOutlet var currentTime: UILabel!
    
    @IBOutlet var btnNext: UIButton!
    @IBAction func changeSlider(_ sender: Any) {
    }
    @IBOutlet var sliderTime: UISlider!
    
    @IBAction func nextMusic(_ sender: Any) {
        changeMusic(indexAudio: 1)
        player?.play()
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: Selector("update"), userInfo: nil, repeats: true)
    }
    
    
    
    @IBAction func upSlider(_ sender: Any) {
        let duration : CMTime = CMTimeMake(Int64(sliderTime.value), 1)
        
        player?.currentItem?.seek(to: duration)
    }
    
    @IBAction func playMusic(_ sender: Any) {
        if player?.rate == 0
        {
            player!.play()
            btnPlay.setTitle("Pause", for: UIControlState.normal)
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: Selector("update"), userInfo: nil, repeats: true)

        } else {
            
            timer.invalidate()
            player!.pause()
            btnPlay.setTitle("Play", for: UIControlState.normal)
        }
        
    }
    @IBOutlet var btnPrev: UIButton!
    @IBOutlet var endTime: UILabel!
    @IBOutlet var btnPlay: UIButton!
    @IBAction func prevMusic(_ sender: Any) {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: Selector("update"), userInfo: nil, repeats: true)
        changeMusic(indexAudio: 2)
        player?.play()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let url = URL(string: listAudio[indexAudio])
        let playerItem:AVPlayerItem = AVPlayerItem(url: url!)
        player = AVPlayer(playerItem: playerItem)
        
        let duration : CMTime = (player?.currentItem?.asset.duration)!
        
        let seconds : Float64 = CMTimeGetSeconds(duration)
        
        count = Int(seconds)
        count+=1
        
        endTime.text = sumTime(time: seconds)
        sliderTime!.maximumValue = Float(seconds);
    }
    
    func changeMusic(indexAudio: Int){
        let url = URL(string: listAudio[indexAudio])
        let playerItem:AVPlayerItem = AVPlayerItem(url: url!)
        player = AVPlayer(playerItem: playerItem)
        
        let duration : CMTime = (player?.currentItem?.asset.duration)!
        
        let seconds : Float64 = CMTimeGetSeconds(duration)
        
        count = Int(seconds)
        count+=1
        
        endTime.text = sumTime(time: seconds)
    }
    
    
    func update() {
        if(count > 0) {
            var indexSlider = sliderTime.value
            indexSlider+=1
            let duration : CMTime = (player?.currentItem?.currentTime())!
            let seconds : Int = Int(CMTimeGetSeconds(duration))
            currentTime.text = currentTime(time: seconds)
            count-=1
            indexTime+=1
            sliderTime.value = Float(seconds)
            
        }
    }
    
    
    func currentTime(time: Int) -> String {
        var strSum = "";
        
        var strHour = "";
        var strMinute = "";
        var strSecond = "";
        
        
        var hour: Int = 0;
        var minute: Int = 0;
        var second: Int = 0;
        
        hour = Int(time)/60/60
        minute = Int(time)/60;
        second = (Int(time))%60;
        
        if(hour == 0){
            strHour = "00:"
        }else{
            strHour = String(hour)+":"
        }
        while minute > 60 {
            minute%=60
        }
        if(minute == 0){
            strMinute = "00:"
        }else{
            strMinute = String(minute)+":"
        }
        if(second == 0){
            strSecond = "00"
        }else if(second < 10){
            strSecond = "0"+String(second)
        }else {
            strSecond = String(second)+""
        }
        
        
        strSum = strHour + strMinute + strSecond
        return strSum
    }
    func sumTime(time: Float64)->String{
        var strSum = "";
        
        var strHour = "";
        var strMinute = "";
        var strSecond = "";
        
        
        var hour: Int = 0;
        var minute: Int = 0;
        var second: Int = 0;
        
        hour = Int(time)/60/60
        minute = Int(time)/60;
        second = (Int(time))%60;
        
        if(hour == 0){
            strHour = "00:"
        }else{
            strHour = String(hour)+":"
        }
        if(minute == 0){
            strMinute = "00:"
        }else{
            strMinute = String(minute)+":"
        }
        if(second == 0){
            strSecond = "00"
        }else if(second < 10){
            strSecond = "0"+String(second)
        }else {
            strSecond = String(second)+""
        }
        
        
        strSum = strHour + strMinute + strSecond
        return strSum
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

