package mind.audio.mini.miniaudio.webapi;

/**
 * Created by Dev on 11/29/2016.
 */
import android.os.StrictMode;

import java.io.File;
import java.io.IOException;

import okhttp3.Authenticator;
import okhttp3.Credentials;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.Route;

public class ApiUtility {
    private static final MediaType MEDIA_TYPE_PNG = MediaType.parse("image/png");
    public static final String HTTP_POST = "HTTP_POST";
    public static final String HTTP_GET = "HTTP_GET";

    private static OkHttpClient client = new OkHttpClient();

    private static String okHttpGet(String url, final String user,final String pass) throws IOException {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Request request = new Request.Builder()
                .url(url)
                .build();

        if (user != null && pass != null) {
            client = new OkHttpClient.Builder()
                    .authenticator(new Authenticator() {
                        @Override public Request authenticate(Route route, Response response) throws IOException {
                            String credential = Credentials.basic(user, pass);
                            return response.request().newBuilder()
                                    .header("Authorization", credential)
                                    .build();
                        }
                    })
                    .build();
        }

        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    private static String okHttpPost(String url, final String user,final String pass, MultipartBody.Builder bodyBuilder) throws IOException {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);
        String[] urlParts = url.split("\\?");
        if (urlParts.length == 0) {
            return "";
        }

        RequestBody body = null;

        Request request = null;
        if (bodyBuilder != null) {
            body = bodyBuilder.build();
            request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
        } else {
            String baseUrl = urlParts[0];
            FormBody.Builder builder = new FormBody.Builder();
            if (urlParts.length > 1 && urlParts[1].length() > 0) {
                String[] parameterParts = urlParts[1].split("&");
                if (parameterParts.length > 0) {
                    for (int index = 0; index < parameterParts.length; index++) {
                        String[] pair = parameterParts[index].split("=");
                        if (pair.length == 2) {
                            String name = pair[0];
                            String value = pair[1];
                            builder.add(name, value);
                        }
                    }
                }
            }
            body = builder.build();
            request = new Request.Builder()
                    .url(baseUrl)
                    .post(body)
                    .build();
        }

        if (user != null && pass != null) {
            client = new OkHttpClient.Builder()
                    .authenticator(new Authenticator() {
                        @Override public Request authenticate(Route route, Response response) throws IOException {
                            String credential = Credentials.basic(user, pass);
                            return response.request().newBuilder()
                                    .header("Authorization", credential)
                                    .build();
                        }
                    })
                    .build();
        }

        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    private static String okHttpPostFileUpload(String url, final String user,final String pass, File... files) throws IOException {
        MultipartBody.Builder bodyBuilder = new MultipartBody.Builder();
        for (File file : files) {
            bodyBuilder.addFormDataPart("file", file.getName(),
                    RequestBody.create(MediaType.parse("image/png"), file));
        }
        return okHttpPost(url, user, pass, bodyBuilder);
    }

    public static String apiSendFile(String userAuth, String passwordAuth, String URL, File[] files, String... params) throws
            IOException {
        String createdURL = String.format(URL, params);
        return okHttpPostFileUpload(createdURL, userAuth, passwordAuth, files);
    }

    public static String api(String action, String userAuth, String passwordAuth, String URL, String... params) throws
            IOException {
        String createdURL = String.format(URL, params);
        if (action.equals(HTTP_GET)) {
            return okHttpGet(createdURL, userAuth, passwordAuth);
        } else if (action.equals(HTTP_POST)) {
            return okHttpPost(createdURL, userAuth, passwordAuth, null);
        }
        return "";
    }

    public static class WebserviceFailedException extends Exception { }

}
