package mind.audio.mini.miniaudio.webapi;

/**
 * Created by Dev on 11/29/2016.
 */
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GeneralApi {
    protected static final String USERNAME_AUTH = "admin";
    protected static final String PASSWORD_AUTH = "123456";

    protected static void unwrapApiResponseArrayCallback(ApiListener apiListener, String response) {
        ApiResponse resultResponse = new ApiResponse();
        try {
            JSONObject jsonResponse = new JSONObject(response);
            resultResponse.statusCode = jsonResponse.getString("statusCode");
            resultResponse.message = jsonResponse.getString("message");
            resultResponse.dataAsString = jsonResponse.getString("data");
            resultResponse.dataArray = new JSONArray(resultResponse.dataAsString);
            resultResponse.resultSuccess = jsonResponse.getBoolean("success");

        } catch (JSONException e) {
            resultResponse.resultSuccess = false;
        }

        if (resultResponse.resultSuccess) {
            apiListener.success(resultResponse);
        } else {
            apiListener.failure(resultResponse.message);
        }
    }

    protected static void unwrapApiResponseObjectCallback(ApiListener apiListener, String response) {
        ApiResponse resultResponse = new ApiResponse();
        try {
            JSONObject jsonResponse = new JSONObject(response);
            resultResponse.statusCode = jsonResponse.getString("statusCode");
            resultResponse.message = jsonResponse.getString("message");
            resultResponse.dataAsString = jsonResponse.getString("data");
            resultResponse.resultSuccess = jsonResponse.getBoolean("success");
            resultResponse.dataObject = new JSONObject(resultResponse.dataAsString);

        } catch (JSONException e) {
            resultResponse.resultSuccess = false;
        }

        if (resultResponse.resultSuccess) {
            apiListener.success(resultResponse);
        } else {
            apiListener.failure(resultResponse.message);
        }
    }

    public static class ApiResponse {
        public String statusCode;
        public String message;
        public boolean resultSuccess;
        public JSONObject dataObject;
        public JSONArray dataArray;
        public String dataAsString;
    }

    public interface ApiListener {
        public void success(ApiResponse response);
        public void failure(String error);
    }

}
