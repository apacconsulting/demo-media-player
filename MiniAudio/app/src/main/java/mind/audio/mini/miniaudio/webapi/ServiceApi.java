package mind.audio.mini.miniaudio.webapi;

import java.io.IOException;

/**
 * Created by Dev on 11/29/2016.
 */

public class ServiceApi extends GeneralApi {
    private static String URL = "http://appeducation.esy.es/mini/";
    private static final String WEBSERVICE_LINK = URL + "/CreateUser?id=%s&email=%s&name=%s&gender=%s&cloud_id=%s&online=%s";
    public static void apiLoadChat(ApiListener apiListener,String id) {
        ApiResponse apiResponse = new ApiResponse();
        try {
            String response = ApiUtility.api(ApiUtility.HTTP_GET, USERNAME_AUTH, PASSWORD_AUTH, WEBSERVICE_LINK,id);
            unwrapApiResponseArrayCallback(apiListener, response);

        } catch (IOException e) {
            apiResponse.resultSuccess = false;
            apiListener.failure(e.getMessage());
        }
    }
}
