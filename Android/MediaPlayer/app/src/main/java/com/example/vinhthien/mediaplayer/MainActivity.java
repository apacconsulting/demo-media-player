package com.example.vinhthien.mediaplayer;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,OnCompletionListener,SeekBar.OnSeekBarChangeListener{

    private TextView timeStart,endStart;
    private Button btnPlay,btnPrev,btnNext;
    private MediaPlayer mediaPlayer;
    private SeekBar seekBar;
    private int indexFirst;
    private CountDownTimer countDownTimer;
    private List<String> listUri;
    private Uri uri;
    private int IndexAudio = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        timeStart = (TextView) findViewById(R.id.startTime);
        endStart = (TextView) findViewById(R.id.endTime);
        btnPlay = (Button) findViewById(R.id.play);
        btnNext = (Button) findViewById(R.id.next);
        btnPrev = (Button) findViewById(R.id.prev);
        seekBar = (SeekBar) findViewById(R.id.timeload);
        listUri = new ArrayList<String>();
        listUri.add("http://appeducation.esy.es/nhac.mp3");
        listUri.add("http://appeducation.esy.es/nhac1.mp3");
        listUri.add("http://appeducation.esy.es/nhac2.mp3");


        seekBar.setOnSeekBarChangeListener(this);
        btnPlay.setOnClickListener(this);
        btnNext.setOnClickListener(this);
        btnPrev.setOnClickListener(this);
        mediaPlayer = new MediaPlayer();

        uri = Uri.parse(listUri.get(IndexAudio).toString());
        try {
            mediaPlayer.setOnCompletionListener(this);
            mediaPlayer.setDataSource(this, uri);
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.prepare();
            indexFirst = mediaPlayer.getDuration()/1000;
            endStart.setText(Time(mediaPlayer.getDuration()));

        } catch (IOException e) {
            e.printStackTrace();
        }
        countDownTimer = new CountDownTimer(mediaPlayer.getDuration() ,
                1000) {
            @Override
            public void onTick(long l) {
                seekBar.setMax(mediaPlayer.getDuration()/1000);
                int i = seekBar.getProgress();
                i++;
                timeStart.setText(currentTime(mediaPlayer.getCurrentPosition()/1000));
                seekBar.setProgress(mediaPlayer.getCurrentPosition()/1000);
            }

            @Override
            public void onFinish() {
                countDownTimer.cancel();
            }
        };
    }

    public void changeAudio(int i){
        mediaPlayer.stop();
        mediaPlayer = new MediaPlayer();

        uri = Uri.parse(listUri.get(i).toString());
        try {
            mediaPlayer.setOnCompletionListener(this);
            mediaPlayer.setDataSource(this, uri);
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.prepare();
            indexFirst = mediaPlayer.getDuration()/1000;
            endStart.setText(Time(mediaPlayer.getDuration()));

        } catch (IOException e) {
            e.printStackTrace();
        }
        countDownTimer = new CountDownTimer(mediaPlayer.getDuration() ,
                1000) {
            @Override
            public void onTick(long l) {
                seekBar.setMax(mediaPlayer.getDuration()/1000);
                int i = seekBar.getProgress();
                i++;
                timeStart.setText(currentTime(mediaPlayer.getCurrentPosition()/1000));
                seekBar.setProgress(mediaPlayer.getCurrentPosition()/1000);
            }

            @Override
            public void onFinish() {
                countDownTimer.cancel();
            }
        }.start();
        mediaPlayer.start();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.play:
                if(btnPlay.getTag().equals("0")){
                    mediaPlayer.start();
                    countDownTimer.start();
                    btnPlay.setText("Pause");
                    btnPlay.setTag("1");
                }else{
                    countDownTimer.cancel();
                    mediaPlayer.pause();
                    btnPlay.setText("Play");
                    btnPlay.setTag("0");
                }
                break;
            case R.id.prev:
                btnPlay.setText("Pause");
                btnPlay.setTag("1");
                countDownTimer.cancel();
                seekBar.setProgress(0);
                if(IndexAudio == 0){
                    changeAudio(listUri.size()-1);
                }else{
                    IndexAudio--;
                    changeAudio(IndexAudio);
                }
                break;
            case R.id.next:
                btnPlay.setText("Pause");
                btnPlay.setTag("1");
                countDownTimer.cancel();
                seekBar.setProgress(0);
                if(IndexAudio == (listUri.size()-1)){
                    IndexAudio = 0;
                    changeAudio(IndexAudio);
                }else{
                    IndexAudio++;
                    changeAudio(IndexAudio);
                }
                break;
            default:
                break;
        }
    }

    public void changeCountDown(){
        countDownTimer = new CountDownTimer(mediaPlayer.getDuration() ,
                1000) {
            @Override
            public void onTick(long l) {
                seekBar.setMax(mediaPlayer.getDuration()/1000);
                int i = seekBar.getProgress();
                i++;
                timeStart.setText(currentTime(mediaPlayer.getCurrentPosition()/1000));
                seekBar.setProgress(mediaPlayer.getCurrentPosition()/1000);
            }

            @Override
            public void onFinish() {
                countDownTimer.cancel();
            }
        }.start();
    }
    private String currentTime(int time){
        String Sumtime = "";
        String strHour = "";
        String strMinute = "";
        String strSecond = "";

        int hour = 0;
        int minute = 0;
        int second = 0;

        hour = time/60/60;
        minute = time/60;
        second = time%60;
        if(hour == 0){
            strHour = "00:";
        }else{
            strHour = hour+":";
        }


        while (minute > 60){
            minute%=60;
        }

        if(minute == 0){
            strMinute ="00:";
        }else {
            strMinute =minute+":";
        }

        if(second == 0){
            strSecond ="00";
        }if(second < 10){
            strSecond ="0"+second;
        }else {
            strSecond =second+"";
        }
        Sumtime = strHour+strMinute+strSecond;
        return Sumtime;

    }

    private String Time(int time){
        String Sumtime = "";
        String strHour = "";
        String strMinute = "";
        String strSecond = "";

        int hour = 0;
        int minute = 0;
        int second = 0;

        hour = time/60/60/1000;
        minute = time/60/1000;
        second = (time/1000)%60;
        if(hour == 0){
            strHour = "00:";
        }else{
            strHour = hour+":";
        }
        if(minute == 0){
            strMinute ="00:";
        }else{
            strMinute = minute+":";
        }
        if(second == 0){
            strSecond = "00";
        }else if(second < 10){
            strSecond ="0"+second;
        }else {
            strSecond =second+"";
        }
        Sumtime = strHour+strMinute+strSecond;
        return Sumtime;

    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        btnPlay.setText("Play");
        btnPlay.setTag("0");
        seekBar.setProgress(0);
        //mediaPlayer.stop();
        countDownTimer.onFinish();

    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        //timeStart.setText(currentTime(seekBar.getProgress()));
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        mediaPlayer.seekTo(seekBar.getProgress()*1000);
        countDownTimer.cancel();
        changeCountDown();

    }
}
